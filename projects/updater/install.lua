local args = {...}

local url = "http://lua.gaywine.org"

local path = "bin/"
if not fs.exists(path) then
    fs.makeDir(path)
end
local temp = "temp/"
if not fs.exists(temp) then
    fs.makeDir(temp)
end

local programToInstall = nil
if #args > 0 then
    programToInstall = args[1]
else
    print("usage:")
    print("install.lua <project>")
    print("install.lua <project> <custom install path>")
end

local customPath = nil
if #args > 1 then
    print("WARNING: custom paths are currently not supported.")
    customPath = shell.resolve(args[2])
end

local function downloadFile(program, filename)
    print("downloading " .. filename .. "...")
    local fileUrl = url .. "/files/" .. program .. "/" .. filename

    local result = http.get(fileUrl)
    local resultText = result.readAll()

    local file = fs.open(temp .. filename, "w")
    file.write(resultText)
    file.close()
end

local function split(pString, pPattern)
    local Table = {} -- NOTE: use {n = 0} in Lua-5.0
    local fpat = "(.-)" .. pPattern
    local last_end = 1
    local s, e, cap = pString:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= "" then
            table.insert(Table, cap)
        end
        last_end = e + 1
        s, e, cap = pString:find(fpat, last_end)
    end
    if last_end <= #pString then
        cap = pString:sub(last_end)
        table.insert(Table, cap)
    end
    return Table
end

local function getAllFiles(program)
    local indexesUrl = url .. "/indexes/" .. program

    local result = http.get(indexesUrl)
    if result == nil then
        print("error! getting indexes for " .. program)
        error()
    end
    local resultText = result.readAll()
    local resultTable = split(resultText, "\n")

    local indexesFile = fs.open(path .. program .. "/indexes.dat", "w")
    indexesFile.write(resultText)
    indexesFile.close()
    print("wrote indexes for " .. program)

    local programsFile = fs.open(path .. "programs.dat", "a")
    programsFile.write(program)
    programsFile.close()

    local finalResult = {}
    for key, value in ipairs(resultTable) do
        finalResult[key] = split(value, " ")[1]
    end

    return finalResult
end

local function finishInstall(program, files)
    if files == nil then
        print("files var is nil")
        error()
    end
    for key, file in ipairs(files) do
        if customPath ~= nil then
            print("moving " .. file .. " to " .. customPath .. "...")
            if not fs.exists(customPath) then
                fs.makeDir(customPath)
            end
            if fs.exists(customPath .. "/" .. file) then
                fs.delete(customPath .. "/" .. file)
            end
            fs.move(temp .. file, customPath .. "/" .. file)
        else
            print("moving " .. file .. " to " .. path .. program .. "...")
            if not fs.exists(path .. program) then
                fs.makeDir(path .. program)
            end
            if fs.exists(path .. program .. "/" .. file) then
                fs.delete(path .. program .. "/" .. file)
            end
            fs.move(temp .. file, path .. program .. "/" .. file)
        end
    end

    if customPath ~= nil then
        local program = "shell.run(" .. customPath .. "/run.lua)"
        file = fs.open(path .. program .. "/run.lua")
        file.write(program)
        file.close()
    end

    fs.delete(temp)
end

if not fs.exists(path .. "programs.dat") then
    local file = fs.open(path .. "programs.dat", "w")
    file.write("")
    file.close()
end
local programsFile = fs.open(path .. "programs.dat", "r")
local programsText = programsFile.readAll()
local programsTable = split(programsText, "\n")
local shouldInstall = true
for _, value in ipairs(programsTable) do
    if value == programToInstall then
        print(programToInstall .. " is already installed. checking for updates instead...")
        shouldInstall = false
    end
end
if shouldInstall then
    local files = getAllFiles(programToInstall)
    for _, value in ipairs(files) do
        if value ~= nil then
            downloadFile(programToInstall, value)
        end
    end
    finishInstall(programToInstall, files)
elseif fs.exists(path .. "updater/update.lua") then
    shell.run(path .. "updater/update.lua " .. programToInstall)
end
