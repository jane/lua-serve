args = {...}

url = "http://lua.gaywine.org"

path = "bin/"
if not fs.exists(path) then
    fs.makeDir(path)
end
temp = "temp/"
if not fs.exists(temp) then
    fs.makeDir(temp)
end

local programToInstall = nil
if #args > 0 then
    programToInstall = args[1]
end

local customPath = nil
if #args > 1 then
    print("WARNING: custom paths are currently not supported.")
    customPath = shell.resolve(args[2])
end

local function downloadFile(program, filename)
    print("downloading " .. filename .. "...")
    local fileUrl = url .. "/files/" .. program .. "/" .. filename

    local result = http.get(fileUrl)
    local resultText = result.readAll()

    local file = fs.open(temp .. filename, "w")
    file.write(resultText)
    file.close()
end

local function split(pString, pPattern)
    local Table = {} -- NOTE: use {n = 0} in Lua-5.0
    local fpat = "(.-)" .. pPattern
    local last_end = 1
    local s, e, cap = pString:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= "" then
            table.insert(Table, cap)
        end
        last_end = e + 1
        s, e, cap = pString:find(fpat, last_end)
    end
    if last_end <= #pString then
        cap = pString:sub(last_end)
        table.insert(Table, cap)
    end
    return Table
end

local function getAllFiles(program)
    local indexesUrl = url .. "/indexes/" .. program
    local updateUrl = url .. "/updates/" .. program

    local currentFiles = ""
    if fs.exists(path .. program .. "/indexes.dat") then
        print("#DEBUG path " .. path .. program .. "/indexes.dat")
        local indexesFile = fs.open(path .. program .. "/indexes.dat", "r")
        local indexesFileText = indexesFile.readAll()
        print("#DEBUG indexesFileText " .. indexesFileText)
        currentFiles = indexesFileText
        indexesFile.close()
    end

    print("#DEBUG currentFiles " .. currentFiles)

    local updateResult = http.post(updateUrl, "content=" .. textutils.urlEncode(currentFiles))
    if updateResult == nil then
        print("error! getting updates for " .. program)
        error()
    end
    local updateText = updateResult.readAll()
    print("#DEBUG updateText" .. updateText)
    local updateTable = split(updateText, "\n")

    local indexesResult = http.get(indexesUrl)
    if indexesResult == nil then
        print("error! getting indexes for " .. program)
        error()
    end
    local indexesText = indexesResult.readAll()
    print("#DEBUG indexesText" .. indexesText)
    local indexesTable = split(indexesText, "\n")

    local newIndexesFile = fs.open(path .. program .. "/indexes.dat", "w")
    newIndexesFile.write(indexesText)
    newIndexesFile.close()
    print("wrote new indexes for " .. program)

    local finalResult = {}
    for key, value in ipairs(indexesTable) do
        local filename = split(value, " ")[1]
        for key2, value2 in ipairs(updateTable) do
            local valueTable = split(value2, " ")
            if filename == valueTable[1] then
                local filePath = path .. program .. "/" .. valueTable[1]
                if valueTable[2] == "delete" and fs.exists(filePath) then
                    fs.delete(filePath)
                elseif valueTable[2] == "download" then
                    finalResult[#finalResult + 1] = valueTable[1]
                end
            end
        end
    end

    return finalResult
end

local function finishInstall(program, files)
    if files == nil then
        print("files var is nil")
        error()
    end
    for _, file in ipairs(files) do
        if customPath ~= nil then
            print("moving " .. file .. " to " .. customPath .. "...")
            if not fs.exists(customPath) then
                fs.makeDir(customPath)
            end
            if fs.exists(customPath .. "/" .. file) then
                fs.delete(customPath .. "/" .. file)
            end
            fs.move(temp .. file, customPath .. "/" .. file)
        else
            print("moving " .. file .. " to " .. path .. program .. "...")
            if not fs.exists(path .. program) then
                fs.makeDir(path .. program)
            end
            if fs.exists(path .. program .. "/" .. file) then
                fs.delete(path .. program .. "/" .. file)
            end
            fs.move(temp .. file, path .. program .. "/" .. file)
        end
    end

    if customPath ~= nil then
        local program = "shell.run(" .. customPath .. "/run.lua)"
        file = fs.open(path .. program .. "/run.lua")
        file.write(program)
        file.close()
    end

    fs.delete(temp)
end

if programToInstall == nil then
    print("no program specified, updating all...")
    local programsFile = fs.open(path .. "programs.dat", "r")
    local programsText = programsFile.readAll()
    local programsTable = split(programsText, "\n")
    for _, value in ipairs(programsTable) do
        if value ~= "" and value ~= nil then
            print("checking for updates for " .. value)
            local files = getAllFiles(value)

            if #files == 0 then
                print("no updates found.")
            elseif files ~= nil then
                for _, value2 in ipairs(files) do
                    if value2 ~= nil then
                        downloadFile(value, value2)
                    end
                end
                finishInstall(value, files)
            end
        end
    end
else
    print("getting updates for " .. programToInstall .. "...")
    local files = getAllFiles(programToInstall)

    if #files == 0 then
        print("no updates found.")
    else
        for _, value in ipairs(files) do
            if value ~= nil then
                downloadFile(programToInstall, value)
            end
        end
        finishInstall(programToInstall, files)
    end
end
